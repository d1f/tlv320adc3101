#ifndef __ioctl_H__
#define __ioctl_H__

#define IOCTL_GET_NUM_CTRLS	0		// Get total number of controls
#define IOCTL_GET_CTRL_DESC	1		// Get description of controls
#define IOCTL_GET_CTRL_VALUE	2		// Get current values of controls
#define IOCTL_GET_CTRL_MENU	3		// Get exist menu items of control
#define IOCTL_SET_CTRL_VALUE	4		// Set current volues of controls

#define IOCTL_ID(id)		((id) & 0xFFFFFF)
#define IOCTL_CMD(cmd)		(((cmd)>>24) & 0xFF)
#define IOCTL_NUM(cmd,id)	(((id) & 0xFFFFFF) | (((cmd)<<24)&0xFF000000))

#define MAX_NAME_LEN		32
#define MAX_DESC_LEN		128
#define MAX_UNIT_LEN		8

struct menu_items {
	char name[MAX_NAME_LEN];
	char description[MAX_DESC_LEN];
	int ctrl_value;
};

#define FLAG_RO			0x00000001
#define FLAG_MENU		0x00000002

struct control {
	unsigned int id;
	char name[MAX_NAME_LEN];
	char description[MAX_DESC_LEN];
	char unit[MAX_UNIT_LEN];
	unsigned int flags;
	int min_value, max_value, def_value, value;
	int step;
	int numerator, denominator;
	struct menu_items *items;
};

#endif // __ioctl_H__
