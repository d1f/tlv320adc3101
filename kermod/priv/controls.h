#ifndef __controls_H__
#define __controls_H__

#include "ioctl.h"
//#define ARRAY_SIZE(array) (sizeof(array)/sizeof(array[]))

struct menu_items inputs[] = {
	[0] = {.name="single", .ctrl_value=0},
	[1] = {.name="diff"  , .ctrl_value=1},
	[2] = {.name="line"  , .ctrl_value=2}};

struct menu_items micbias[] = {
	[0] = {.name="off", .ctrl_value=0},
	[1] = {.name="2"  , .ctrl_value=1},
	[2] = {.name="2.5", .ctrl_value=2},
	[3] = {.name="3.3", .ctrl_value=3}};

struct menu_items on_off[] = {
	[0] = {.name="off", .ctrl_value=0},
	[1] = {.name="on" , .ctrl_value=1}};

struct menu_items agc_levels[] = {
	[0] = {.name="-5.5", .ctrl_value=0},
	[1] = {.name="-8"  , .ctrl_value=1},
	[2] = {.name="-10" , .ctrl_value=2},
	[3] = {.name="-12" , .ctrl_value=3},
	[4] = {.name="-14" , .ctrl_value=4},
	[5] = {.name="-17" , .ctrl_value=5},
	[6] = {.name="-20" , .ctrl_value=6},
	[7] = {.name="-24" , .ctrl_value=7}};

struct menu_items agc_hyst[] = {
	[0] = {.name="1", .ctrl_value=0},
	[1] = {.name="2", .ctrl_value=1},
	[2] = {.name="3", .ctrl_value=2},
	[3] = {.name="disable", .ctrl_value=3}};

struct menu_items agc_noise_debounce[32] = {
	[ 0] = {.name="0"   , .ctrl_value= 0},
	[ 1] = {.name="0.25", .ctrl_value= 1},
	[ 2] = {.name="0.5" , .ctrl_value= 2},
	[ 3] = {.name="1"   , .ctrl_value= 3},
	[ 4] = {.name="2"   , .ctrl_value= 4},
	[ 5] = {.name="4"   , .ctrl_value= 5},
	[ 6] = {.name="8"   , .ctrl_value= 6},
	[ 7] = {.name="16"  , .ctrl_value= 7},
	[ 8] = {.name="32"  , .ctrl_value= 8},
	[ 9] = {.name="64"  , .ctrl_value= 9},
	[10] = {.name="128" , .ctrl_value=10},
	[11] = {.name="256" , .ctrl_value=11},
	[12] = {.name="512" , .ctrl_value=12},
	[13] = {.name="768" , .ctrl_value=13},
	[14] = {.name="1024", .ctrl_value=14},
	[15] = {.name="1280", .ctrl_value=15},
	[16] = {.name="1536", .ctrl_value=16},
	[17] = {.name="1792", .ctrl_value=17},
	[18] = {.name="2048", .ctrl_value=18},
	[19] = {.name="2304", .ctrl_value=19},
	[20] = {.name="2560", .ctrl_value=20},
	[21] = {.name="2816", .ctrl_value=21},
	[22] = {.name="3072", .ctrl_value=22},
	[23] = {.name="3328", .ctrl_value=23},
	[24] = {.name="3584", .ctrl_value=24},
	[25] = {.name="3840", .ctrl_value=25},
	[26] = {.name="4096", .ctrl_value=26},
	[27] = {.name="4352", .ctrl_value=27},
	[28] = {.name="4608", .ctrl_value=28},
	[29] = {.name="4864", .ctrl_value=29},
	[30] = {.name="5120", .ctrl_value=30},
	[31] = {.name="5376", .ctrl_value=31},
};

struct menu_items agc_signal_debounce[16] = {
	[ 0] = {.name="0"   , .ctrl_value= 0},
	[ 1] = {.name="0.25", .ctrl_value= 1},
	[ 2] = {.name="0.5" , .ctrl_value= 2},
	[ 3] = {.name="1"   , .ctrl_value= 3},
	[ 4] = {.name="2"   , .ctrl_value= 4},
	[ 5] = {.name="4"   , .ctrl_value= 5},
	[ 6] = {.name="8"   , .ctrl_value= 6},
	[ 7] = {.name="16"  , .ctrl_value= 7},
	[ 8] = {.name="32"  , .ctrl_value= 8},
	[ 9] = {.name="64"  , .ctrl_value= 9},
	[10] = {.name="128" , .ctrl_value=10},
	[11] = {.name="256" , .ctrl_value=11},
	[12] = {.name="384" , .ctrl_value=12},
	[13] = {.name="512" , .ctrl_value=13},
	[14] = {.name="640" , .ctrl_value=14},
	[15] = {.name="768" , .ctrl_value=15},
};

struct control controls[] = {
	[0] = {.id=0, .name="input", .unit="", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(inputs)-1, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=inputs},

	[1] = {.id=1, .name="gain", .unit="dB", .flags=0,
		.min_value=0, .max_value=80, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=2, .items=NULL},

	[2] = {.id=2, .name="-6dB", .unit="", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(on_off)-1, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=on_off},

	[3] = {.id=3, .name="volume", .unit="dB", .flags=0,
		.min_value=-24, .max_value=40, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=2, .items=NULL},

	[4] = {.id=4, .name="mute", .unit="", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(on_off)-1, .def_value=1, .value=1,
		.step=1, .numerator=1, .denominator=1, .items=on_off},

	[5] = {.id=5, .name="micbias", .unit="V", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(micbias)-1, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=micbias},

	[6] = {.id=6, .name="AGC", .unit="", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(on_off)-1, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=on_off},

	[7] = {.id=7, .name="AGC_level", .unit="dB", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(agc_levels)-1, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=agc_levels},

	[8] = {.id=8, .name="AGC_hyst", .unit="dB", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(agc_hyst)-1, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=agc_hyst},

	[9] = {.id=9, .name="AGC_noise_dis", .unit="", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(on_off)-1, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=on_off},

	[10] = {.id=10, .name="AGC_noise_level", .unit="dB", .flags=0,
		.min_value=-90, .max_value=-30, .def_value=-90, .value=-90,
		.step=2, .numerator=1, .denominator=1, .items=NULL},

	[11] = {.id=11, .name="AGC_max_gain", .unit="dB", .flags=0,
		.min_value=0, .max_value=80, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=2, .items=NULL},

	[12] = {.id=12, .name="AGC_gain", .unit="dB", .flags=FLAG_RO,
		.min_value=-24, .max_value=80, .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=2, .items=NULL},

	[13] = {.id=13, .name="AGC_attack", .unit="ms", .flags=0,
		.min_value=2, .max_value=16128, .def_value=2, .value=2,
		.step=2, .numerator=1, .denominator=1, .items=NULL},

	[14] = {.id=14, .name="AGC_decay", .unit="ms", .flags=0,
		.min_value=32, .max_value=258048, .def_value=32, .value=32,
		.step=2, .numerator=1, .denominator=1, .items=NULL},

	[15] = {.id=15, .name="AGC_noise_debounce", .unit="ms", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(agc_noise_debounce), .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=agc_noise_debounce},

	[16] = {.id=16, .name="AGC_signal_debounce", .unit="ms", .flags=FLAG_MENU,
		.min_value=0, .max_value=ARRAY_SIZE(agc_signal_debounce), .def_value=0, .value=0,
		.step=1, .numerator=1, .denominator=1, .items=agc_signal_debounce},

	[17] = {.id=17, .name="DC_offset", .unit="mV", .flags=0,
		.min_value=-7, .max_value=7, .def_value=0, .value=0,
		.step=1, .numerator=15, .denominator=1, .items=NULL},
};

struct channel_device;
typedef int (*control_set_func)(struct channel_device *channel, int index, int value);
typedef int (*control_get_func)(struct channel_device *channel, int index, int *value);

int control_set_input(struct channel_device *channel, int index, int value);
int control_set_dc_offset(struct channel_device *channel, int index, int value);
int control_set_gain(struct channel_device *channel, int index, int value);
int control_set_6dB(struct channel_device *channel, int index, int value);
int control_set_volume(struct channel_device *channel, int index, int value);
int control_set_mute(struct channel_device *channel, int index, int value);
int control_set_micbias(struct channel_device *channel, int index, int value);
int control_set_AGC(struct channel_device *channel, int index, int value);
int control_set_AGC_level(struct channel_device *channel, int index, int value);
int control_set_AGC_hyst(struct channel_device *channel, int index, int value);
int control_set_AGC_noise_dis(struct channel_device *channel, int index, int value);
int control_set_AGC_noise_level(struct channel_device *channel, int index, int value);
int control_set_AGC_max_gain(struct channel_device *channel, int index, int value);
int control_set_AGC_attack(struct channel_device *channel, int index, int value);
int control_set_AGC_decay(struct channel_device *channel, int index, int value);
int control_set_AGC_noise_debounce(struct channel_device *channel, int index, int value);
int control_set_AGC_signal_debounce(struct channel_device *channel, int index, int value);

int control_get_AGC_gain(struct channel_device *channel, int index, int *value);

control_set_func control_set_funcs[ARRAY_SIZE(controls)] = {
	control_set_input,
	control_set_gain,
	control_set_6dB,
	control_set_volume,
	control_set_mute,
	control_set_micbias,
	control_set_AGC,
	control_set_AGC_level,
	control_set_AGC_hyst,
	control_set_AGC_noise_dis,
	control_set_AGC_noise_level,
	control_set_AGC_max_gain,
	NULL,		// control_get_AGC_gain
	control_set_AGC_attack,
	control_set_AGC_decay,
	control_set_AGC_noise_debounce,
	control_set_AGC_signal_debounce,
	control_set_dc_offset,
};

control_get_func control_get_funcs[ARRAY_SIZE(controls)] = {
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	NULL, NULL, NULL, NULL,
	control_get_AGC_gain, NULL, NULL, NULL, NULL, NULL};

#endif // __controls_H__
