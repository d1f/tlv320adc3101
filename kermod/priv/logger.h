#ifndef __loggerH__
#define __loggerH__

#include <linux/kernel.h>

#ifdef DEBUG

extern int log_shift;

#define INCREASE_LOG_SHIFT {printk(KERN_DEBUG "> %*c%s()\n", log_shift*2, ' ', __FUNCTION__); log_shift++;}
#define DECREASE_LOG_SHIFT {log_shift--; printk(KERN_DEBUG "< %*c%s()\n", log_shift*2, ' ', __FUNCTION__);}
#define LOG_DBG(format, ...) printk(KERN_DEBUG "D %*c"format, log_shift*2, ' ', ##__VA_ARGS__)
#define LOG_INFO(format, ...) printk(KERN_INFO "I %*c"format, log_shift*2, ' ', ##__VA_ARGS__)
#define LOG_ERR(format, ...) printk(KERN_ERR "E %*c"format, log_shift*2, ' ', ##__VA_ARGS__)
#define LOG_DBG0(format, ...) printk(KERN_DEBUG format, ##__VA_ARGS__)

#else

#define INCREASE_LOG_SHIFT while(0)
#define DECREASE_LOG_SHIFT while(0)
#define LOG_DBG(format, ...) while(0)
#define LOG_DBG0(format, ...) while(0)

#define LOG_INFO(format, ...) printk(KERN_INFO "%s():"format, __FUNCTION__, ##__VA_ARGS__)
#define LOG_ERR(format, ...) printk(KERN_ERR "%s():"format, __FUNCTION__, ##__VA_ARGS__)

#endif



#endif

