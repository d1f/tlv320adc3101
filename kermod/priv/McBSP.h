#ifndef __McBSP_H__
#define __McBSP_H__

#include <linux/types.h>

#define MCBSP_CLK_NAME "McBSPCLK"

struct McBSP_regs {
	uint32_t DRR;			// Data receive register
	uint32_t DXR;			// Data transmit register
	uint32_t SPCR;			// Serial port control register
	uint32_t RCR;			// Recieve control register
	uint32_t XCR;			// Transmit control register
	uint32_t SRGR;			// Sample rate generator register
	uint32_t MCR;			// Multichannel control register
	uint32_t RCERE0;		// Enchanced receive channel enable register partition A/B
	uint32_t XCERE0;		// Enchanced trancmit channel enable register partition A/B
	uint32_t PCR;			// Pin control register
	uint32_t RCERE1;		// Enchanced receive channel enable register partition C/D
	uint32_t XCERE1;		// Enchanced transmit channel enable register partition C/D
	uint32_t RCERE2;		// Enchanced receive channel enable register partition E/F
	uint32_t XCERE2;		// Enchanced transmit channel enable register partition E/F
	uint32_t RCERE3;		// Enchanced receive channel enable register partition G/H
	uint32_t XCERE4;		// Enchanced transmit channel enable register partition G/H
};

#define SPCR_RRST		0x00000001		// Receiver reset bit resets or enables the receiver
#define SPCR_RRDY		0x00000002		// Receiver ready bit.
#define SPCR_RFULL		0x00000004		// Receive shift register full bit.
#define SPCR_RSYNCERR		0x00000008		// Receive synchronization error bit.
#define SPCR_RINTM(a)		(((a)&3)<<4)		// Receive interrupt (RINT) mode bit.
#define SPCR_DXEN		(1<<7)			// DX enabler bit.
#define SPCR_CLKSTP(a)		(((a)&3)<<11)		// Clock stop mode bit.
#define SPCR_RJUST(a)		(((a)&3)<<13)		// Receive sign-extension and justification mode bit.
#define SPCR_DLB		(1<<15)			// Digital loop back mode enable bit.
#define SPCR_XRST		(1<<16)			// Transmitter reset bit resets or enables the transmitter.
#define SPCR_XRDY		(1<<17)			// Transmitter ready bit.
#define SPCR_XEMPTY		(1<<18)			// Transmitter shift register empty bit.
#define SPCR_XSYNCERR		(1<<19)			// Transmit synchronization error bit.
#define SPCR_XINTM(a)		(((a)&3)<<20)		// Transmit interrupt (XINT) mode bit.
#define SPCR_GRST		(1<<22)			// Sample-rate generator reset bit.
#define SPCR_FRST		(1<<23)			// Frame-sync generator reset bit.
#define SPCR_SOFT		(1<<24)			// Soft bit enable mode bit.
#define SPCR_FREE		(1<<25)			// Free-running enable mode bit.

#define RCR_RWDREVRS		(1<<4)			// Receive 32-bit bit reversal enable bit.
#define RCR_RWDLEN1(a)		(((a)&7)<<5)		// Specifies the receive word length in phase 1.
	#define WDLEN_8		0
	#define WDLEN_12	1
	#define WDLEN_16	2
	#define WDLEN_20	3
	#define WDLEN_24	4
	#define WDLEN_32	5
#define RCR_RFRLEN1(a)		(((a)&0x7F)<<8)		// Specifies the receive frame length in phase 1.
#define RCR_RDATDLY(a)		(((a)&3)<<16)		// Receive data delay bit.
#define RCR_RFIG		(1<<18)			// Receive frame ignore bit.
#define RCR_RCOMPAND(a)		(((a)&3)<<19)		// Receive companding mode bit.
#define RCR_RWDLEN2(a)		(((a)&7)<<21)		// Specifies the receive word length in phase 2.
#define RCR_RFRLEN2(a)		(((a)&0x7F)<<24)	// Specifies the receive frame length in phase 2.
#define RCR_RPHASE		(1<<31)			// Receive phases bit.

#define XCR_XWDREVRS		(1<<4)			// Transmit 32-bit bit reversal enable bit.
#define XCR_XWDLEN1(a)		(((a)&7)<<5)		// Specifies the transmit word length in phase 1.
#define XCR_XFRLEN1(a)		(((a)&0x7F)<<8)		// Specifies the transmit frame length in phase 1.
#define XCR_XDATDLY(a)		(((a)&3)<<16)		// Transmit data delay bit.
#define XCR_XFIG		(1<<18)			// Transmit frame ignore bit.
#define XCR_XCOMPAND(a)		(((a)&3)<<19)		// Transmit companding mode bit.
#define XCR_XWDLEN2(a)		(((a)&7)<<21)		// Specifies the transmit word length in phase 2.
#define XCR_XFRLEN2(a)		(((a)&0x7F)<<24)	// Specifies the transmit frame length in phase 2.
#define XCR_XPHASE		(1<<31)			// Transmit phases bit.

#define SRGR_CLKGDIV(a)		(((a)&0xFF)<<0)		// Sample-rate generator clock devicer value.
#define SRGR_FWID(a)		(((a)&0xFF)<<8)		// Frame width value plus 1.
#define SRGR_FPER(a)		(((a)&0xFFF)<<16)	// Frame period value plus 1.
#define SRGR_FSGM		(1<<28)			// Sample-rate generator transmit frame-synchronization mode bit.
#define SRGR_CLKSM		(1<<29)			// Sample rate generator input clock mode but.
#define SRGR_CLKSP		(1<<30)			// CLKS polarity clock edge select but.
#define SRGR_GSYNC		(1<<31)			// Sample-rate generator clock synchronization bit.

#define PCR_CLKRP		(1<<0)			// Receive clock polarity bit.
#define PCR_CLKXP		(1<<1)			// Transmit clock polarity bit.
#define PCR_FSRP		(1<<2)			// Receive frame-synchronization polarity bit.
#define PCR_FSXP		(1<<3)			// Transmit frame-synchronization polarity bit.
#define PCR_SCLKME		(1<<7)			// Sample rate generator input clock mode bit.
#define PCR_CLKRM		(1<<8)			// Receive clock mode bit.
#define PCR_CLKXM		(1<<9)			// Transmit clock mode bit.
#define PCR_FSRM		(1<<10)			// Receive frame-synchronization mode bit.
#define PCR_FSXM		(1<<11)			// Transmit frame-synchronization mode bit.

#endif // __McBSP_H__
