#ifndef __tlv320adc3101H__
#define __tlv320adc3101H__

int tlv320adc3101_set_input_mode(int channel, int input, int mode);
int tlv320adc3101_set_dc_offset(int channel, int offset);
int tlv320adc3101_set_gain(int channel, int gain);
int tlv320adc3101_set_volume(int channel, int volume);
int tlv320adc3101_set_mute(int channel, int mute);
int tlv320adc3101_set_micbias(int channel, int bias);
int tlv320adc3101_set_AGC(int channel, int mode);
int tlv320adc3101_set_AGC_hyst(int channel, int hyst);
int tlv320adc3101_set_AGC_noise_level(int channel, int level);
int tlv320adc3101_set_AGC_level(int channel, int level);
int tlv320adc3101_set_AGC_max_gain(int channel, int gain);
int tlv320adc3101_set_AGC_attack(int channel, int val);
int tlv320adc3101_set_AGC_decay(int channel, int val);
int tlv320adc3101_set_AGC_noise_debounce(int channel, int debounce);
int tlv320adc3101_set_AGC_signal_debounce(int channel, int debounce);
int tlv320adc3101_get_AGC_gain(int channel, int *gain);
int tlv320adc3101_set_sample_rate(int channel, int sample_rate);

int tlv320adc3101_set_i2c(unsigned char page, unsigned char addr, unsigned char  value);
int tlv320adc3101_get_i2c(unsigned char page, unsigned char addr, unsigned char *value);

#endif // __tlv320adc3101H__

