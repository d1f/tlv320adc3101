#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/delay.h>
#include <linux/i2c.h>
#include "logger.h"
#include "tlv320adc3101.h"

#define VERSION "0.1"
#define DRIVER_NAME "ADC3101"

static unsigned short normal_i2c[] = {0x18, 0x19, 0x1A, 0x1B, I2C_CLIENT_END};
I2C_CLIENT_INSMOD;

int init_codecs = 0;
int master_codec = -1;

static struct i2c_driver tlv320adc3101_driver;

struct codec {
	struct i2c_client client;
} *codecs[4];

int read_byte(struct codec *codec, unsigned char addr) {
	int d = i2c_smbus_read_byte_data(&codec->client, addr);
//	LOG_INFO("i2c read:  0x%02x:%3d => 0x%02x\n", codec->client.addr, addr, d);
	return d;
}
int write_byte(struct codec *codec, unsigned char addr, unsigned char data) {
//	LOG_INFO("i2c write: 0x%02x:%3d <= 0x%02x\n", codec->client.addr, addr, data);
	return i2c_smbus_write_byte_data(&codec->client, addr, data);
}

int tlv320adc3101_set_i2c(unsigned char page, unsigned char addr, unsigned char value)
{
	int rc = write_byte(codecs[0], 0, page);
	if (rc < 0) return rc;

	return write_byte(codecs[0], addr, value);
}

int tlv320adc3101_get_i2c(unsigned char page, unsigned char addr, unsigned char *value)
{
	int rc = write_byte(codecs[0], 0, page);
	if (rc < 0) return rc;
	rc = read_byte(codecs[0], addr);
	if (rc >= 0)
		*value = rc;
	return rc;
}

int tlv320adc3101_set_input_mode(int channel, int input, int mode)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	int reg = 0, pos = 0;
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 1);
	switch(input)
	{
		case 1: reg = 52; pos = 6; break;
		case 2: reg = 52; pos = 2; break;
		case 3: reg = 52; pos = 4; break;
		case 4: reg = 52; pos = 0; break;
		case 5: reg = 54; pos = 4; break;
		case 6: reg = 54; pos = 2; break;
		case 7: reg = 54; pos = 0; break;
	}
	if(channel%2 == 1) reg += 3;
	if(reg != 0) return write_byte(codec, reg, (read_byte(codec, reg) & ~(3<<pos)) | ((mode&3)<<pos));
	else return -EINVAL;
}

int tlv320adc3101_set_dc_offset(int channel, int offset)
{
	int codec_num = channel/2;
	int pos = channel%2==0?4:0;
	int value = offset>=0?(offset&0x7):(((-offset)&0x7)|0x08);
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 1);
	return write_byte(codec, 26, (read_byte(codec, 26) & ~(0xF<<pos)) | ((value&0xF)<<pos));
}

int tlv320adc3101_set_gain(int channel, int gain)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 1);
	if(channel%2 == 0) return write_byte(codec, 59, (read_byte(codec, 59) & ~0x7F) | (gain&0x7F));
	else return write_byte(codec, 60, (read_byte(codec, 60) & ~0x7F) | (gain&0x7F));
}

int tlv320adc3101_set_volume(int channel, int volume)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	if(channel%2 == 0) return write_byte(codec, 83, volume);
	else return write_byte(codec, 84, volume);
}

int tlv320adc3101_set_mute(int channel, int mute)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	if(channel%2 == 0) return write_byte(codec, 82, (read_byte(codec, 82) & ~0x80) | ((mute&1)<<7));
	else return write_byte(codec, 82, (read_byte(codec, 82) & ~0x08) | ((mute&1)<<3));
}

int tlv320adc3101_set_micbias(int channel, int bias)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	int pos = (channel%2)==0?5:3;
	int reg = 51;
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 1);
	return write_byte(codec, reg, (read_byte(codec, reg) & ~(3<<pos)) | ((bias&3)<<pos));
}

int tlv320adc3101_set_AGC(int channel, int mode)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	if(channel%2 == 0) return write_byte(codec, 86, (read_byte(codec, 86) & ~0x80) | ((mode&1)<<7));
	else return write_byte(codec, 94, (read_byte(codec, 94) & ~0x80) | ((mode&1)<<7));
}

int tlv320adc3101_set_AGC_hyst(int channel, int hyst)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	if(channel%2 == 0) return write_byte(codec, 87, (read_byte(codec, 87) & ~0xC0) | ((hyst&3)<<6));
	else return write_byte(codec, 95, (read_byte(codec, 95) & ~0xC0) | ((hyst&3)<<6));
}

int tlv320adc3101_set_AGC_noise_level(int channel, int level)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	if(channel%2 == 0) return write_byte(codec, 87, (read_byte(codec, 87) & ~0x3E) | ((level&0x1F)<<1));
	else return write_byte(codec, 95, (read_byte(codec, 95) & ~0x3E) | ((level&0x1F)<<1));
}

int tlv320adc3101_set_AGC_level(int channel, int level)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	if(channel%2 == 0) return write_byte(codec, 86, (read_byte(codec, 86) & ~0x70) | ((level&7)<<4));
	else return write_byte(codec, 94, (read_byte(codec, 94) & ~0x70) | ((level&7)<<4));
}

int tlv320adc3101_set_AGC_max_gain(int channel, int gain)
{
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	if(channel%2 == 0) return write_byte(codec, 88, gain);
	else return write_byte(codec, 96, gain);
}

int tlv320adc3101_set_AGC_attack(int channel, int val)
{
	int reg = (channel%2)==0?89:97;
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	return write_byte(codec, reg, val);
}

int tlv320adc3101_set_AGC_decay(int channel, int val)
{
	int reg = (channel%2)==0?90:98;
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	return write_byte(codec, reg, val);
}

int tlv320adc3101_set_AGC_noise_debounce(int channel, int debounce)
{
	int reg = (channel%2)==0?91:99;
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	return write_byte(codec, reg, (debounce&0x1F));
}

int tlv320adc3101_set_AGC_signal_debounce(int channel, int debounce)
{
	int reg = (channel%2)==0?92:100;
	int codec_num = channel/2;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) ==0)) return -ENODEV;
	write_byte(codec, 0, 0);
	return write_byte(codec, reg, (debounce&0x1F));
}


int tlv320adc3101_get_AGC_gain(int channel, int *gain)
{
	int reg = (channel%2)==0?93:101;
	int codec_num = channel/2;
	int data;
	struct codec *codec = codecs[codec_num];
	if((codec==NULL) || (channel >= 8) || ((init_codecs & (1<<codec_num)) == 0)) return -ENODEV;
	write_byte(codec, 0, 0);
	data = read_byte(codec, reg);
	if(data & 0x80) *gain = - ((~data & 0xFF)+1);
	else *gain = data;
	return 0;
}

static int set_pll(unsigned short J, unsigned short D, unsigned short R,
		   unsigned short N, unsigned short M, unsigned short AOSR,
		   unsigned short P)
{
    int rc = write_byte(codecs[0], 0, 0);		// Select page 0

#if 0
    pr_info("%s(): J: %hu, D: %hu, R: %hu, N: %hu, M: %hu, AOSR: %hu, P: %hu\n",
	    __func__, J, D, R, N, M, AOSR, P);
#else
    msleep(1);
#endif

    rc |= write_byte(codecs[0],  6, J & 0x3f);		// PLL J-VAL, Don't use 0

    rc |= write_byte(codecs[0],  7, (D >> 8) & 0x3f);	// PLL D, MSB
    rc |= write_byte(codecs[0],  8,  D       & 0xff);	// PLL D, LSB

    R = (R == 16) ? 0 : R;
    P = (P ==  8) ? 0 : P;
    rc |= write_byte(codecs[0],  5,  0x80 | ((P & 0x7) << 4) | (R & 0xf)); // PLL Up, P, R

    N = (N == 128) ? 0 : N;
    rc |= write_byte(codecs[0], 18, 0x80 | (N & 0x7f));	// NADC Up, clock divider

    M = (M == 128) ? 0 : M;
    rc |= write_byte(codecs[0], 19, 0x80 | (M & 0x7f));	// MADC Up, clock divider

    AOSR = (AOSR == 256) ? 0 : AOSR;
    rc |= write_byte(codecs[0], 20, AOSR);		// AOSR

    return rc;
}

int tlv320adc3101_set_sample_rate(int channel, int sample_rate)
{
	/*
	 Fs = (PLLCLK_IN x J.D x R) / (NADC x MADC x AOSR x P)

	            MCLK      J.D      R      N   M   AOSR  P
	 16000 = (24000000 *  7.6800 * 1) / ( 5 * 9 * 128 * 2);
	  8000 = (24000000 *  7.6800 * 1) / (10 * 9 * 128 * 2);
	 22050 = (24000000 * 10.5840 * 1) / ( 5 * 9 * 128 * 2);
	 11025 = (24000000 * 10.5840 * 1) / (10 * 9 * 128 * 2);
	 */

	unsigned short J, D, R, N, M, AOSR, P;

	switch (sample_rate)
	{
	    case 8000:
		J = 7; D = 6800; R = 1; N = 10; M = 9; AOSR = 128; P = 2;
		set_pll(J, D, R, N, M, AOSR, P);
		break;

	    case 16000:
		J = 7; D = 6800; R = 1; N =  5; M = 9; AOSR = 128; P = 2;
		set_pll(J, D, R, N, M, AOSR, P);
		break;

	    case 11025:
		J = 10; D = 5840; R = 1; N = 10; M = 9; AOSR = 128; P = 2;
		set_pll(J, D, R, N, M, AOSR, P);
		break;

	    case 22050:
		J = 10; D = 5840; R = 1; N =  5; M = 9; AOSR = 128; P = 2;
		set_pll(J, D, R, N, M, AOSR, P);
		break;

	    default:
		return -EINVAL;
	}

	return 0;
}

static int codec_reset(struct codec *codec) {
	write_byte(codec, 0, 0);		// Select page 0
	write_byte(codec, 1, 1);		// Software reset
	mdelay(10);
	write_byte(codec, 1, 0);		// Clean software reset
	mdelay(10);
	return 0;
}

static int codec_init(struct codec *codec, int master, int offset) {
	unsigned int wclk_out = master?1:0;
//	unsigned int bclk_out = master?1:0;

	LOG_INFO("TLV320ADC3101 codec init(---, %d, %d)\n", master, offset);

	codec_reset(codec);

/*	write_byte(codec, 4, (1<<2) | (3<<0));		// PLL_CLKIN=ground/BCLK, CODEC_CLKIN=BCLK/PLL_CLK
	write_byte(codec, 5, (1<<4) | (1<<0));		// PLL P & R values
	write_byte(codec, 6, 10);					// PLL J value
	write_byte(codec, 7, D/256);				// PLL D msb value
	write_byte(codec, 8, D%256);				// PLL D lsb value
	write_byte(codec, 18, 10);					// NADC		10
	write_byte(codec, 19, 2);					// MADC		2
	write_byte(codec, 20, 128);					// OASR		128
	write_byte(codec, 21, 188/2);				// IADC		188

	write_byte(codec, 27, (1<<6) | (0<<4) | (0<<3) | (wclk_out<<2) | (1<<0));	// audio interface control 1
			// DSP mode, 16bit, BCLK in, WCLK out, DOUT tristate
	write_byte(codec, 28, offset);				// Data offset

	write_byte(codec, 19, read_byte(codec, 19) | (1<<7));	// Enable MADC devider
	write_byte(codec, 18, read_byte(codec, 18) | (1<<7));	// Enable NADC devider
	write_byte(codec, 5, read_byte(codec, 5) | (1<<7));		// Enable PLL

	write_byte(codec, 81, read_byte(codec, 81) | (1 << 7) | (1<<6));	// Left/Right ADC enable

	write_byte(codec, 0, 1);			// Select page 1
	write_byte(codec, 52, (read_byte(codec, 52) | (0xFF)));
	write_byte(codec, 55, (read_byte(codec, 55) | (0xFF)));
	write_byte(codec, 54, (read_byte(codec, 54) | (0x3F)));
	write_byte(codec, 57, (read_byte(codec, 57) | (0x3F)));
	write_byte(codec, 59, (read_byte(codec, 59) & ~(1<<7)));			// Mute off
	write_byte(codec, 60, (read_byte(codec, 60) & ~(1<<7)));			// Mute off*/

	if(1) {
		{
			const int J = 7;
			const int D = 6800;
			const int R = 1;
			const int P = 2;
			write_byte(codec, 4, (0<<2) | (3<<0));		// PLL_CLKIN=MCLK, CODEC_CLKIN=PLL_CLK, //PLL_CLKIN=ground/BCLK, CODEC_CLKIN=BCLK/PLL_CLK
			write_byte(codec, 5, (P<<4) | (R<<0));		// PLL P & R values
			write_byte(codec, 6, J);					// PLL J value
			write_byte(codec, 7, D>>8);					// PLL D msb value
			write_byte(codec, 8, D&0xFF);				// PLL D lsb value
			//write_byte(codec, 4, (3<<2) | (1<<0));		// PLL_CLKIN=GND, CODEC_CLKIN=BCLK
			write_byte(codec, 18, 5);					// NADC		1
			write_byte(codec, 19, 9);					// MADC		2
			write_byte(codec, 20, 128);					// OASR		250
			//write_byte(codec, 21, 188/2);				// IADC		188
		}

		write_byte(codec, 27, (1<<6) | (0<<4) | (wclk_out<<3) | (wclk_out<<2) | (1<<0));	// audio interface control 1
		// DSP mode, 16bit, BCLK in, WCLK out, DOUT tristate
		write_byte(codec, 28, offset);				// Data offset
		write_byte(codec, 30, 0x80 | 9);			// BCLK N devider

		write_byte(codec, 19, read_byte(codec, 19) | (1<<7));	// Enable MADC devider
		write_byte(codec, 18, read_byte(codec, 18) | (1<<7));	// Enable NADC devider
		write_byte(codec, 5, read_byte(codec, 5) | (1<<7));		// Enable PLL
	}

	//write_byte(codec, 4, (3<<2) | (1<<0));		// PLL_CLKIN=GND, CODEC_CLKIN=BCLK
	//write_byte(codec, 18, 0x80 | 1);			// NADC		1
	//write_byte(codec, 19, 0x80 | 4);			// MADC		2
	//write_byte(codec, 20, 64);					// OASR		250
	//write_byte(codec, 21, 188/2);				// IADC		188
	//write_byte(codec, 27, (1<<6) | (0<<4) | (0<<3) | (0<<2) | (1<<0));	// audio interface control 1
						// DSP mode, 16bit, BCLK in, WCLK in, DOUT tristate
	//write_byte(codec, 28, offset);				// Data offset

	write_byte(codec, 81, read_byte(codec, 81) | (1 << 7) | (1<<6));	// Left/Right ADC enable

	write_byte(codec, 0, 1);			// Select page 1
	write_byte(codec, 52, (read_byte(codec, 52) | (0xFF)));
	write_byte(codec, 55, (read_byte(codec, 55) | (0xFF)));
	write_byte(codec, 54, (read_byte(codec, 54) | (0x3F)));
	write_byte(codec, 57, (read_byte(codec, 57) | (0x3F)));
//	if(master) {
		write_byte(codec, 59, (read_byte(codec, 59) & ~(1<<7)));			// Mute off
		write_byte(codec, 60, (read_byte(codec, 60) & ~(1<<7)));			// Mute off
//	} else {
//		write_byte(codec, 59, (read_byte(codec, 59) | (1<<7)));			// Mute off
//		write_byte(codec, 60, (read_byte(codec, 60) | (1<<7)));			// Mute off
//	}

	LOG_INFO("TLV320ADC3101 codec init finish\n");

	return 0;
}

static int /*__init*/ tlv320adc3101_probe(struct i2c_adapter *bus, int address, int kind) {
	struct codec *codec;
	int ret;
	LOG_INFO("tlv320adc3101_probe: address: 0x%02x\n", address);
	if((address < 0x18) || (address > 0x1B)) return -EINVAL;
	LOG_INFO("tlv320adc3101_probe valid\n");
	init_codecs |= 1 << (address-0x18);
	codec = (struct codec*)kmalloc(sizeof(struct codec), GFP_KERNEL);
	if(codec == 0) {
		ret = -ENOMEM;
		goto err_kmalloc_codec;
	}
	memset(codec, 0, sizeof(struct codec));
	codec->client.addr = address;
	codec->client.adapter = bus;
	codec->client.driver = &tlv320adc3101_driver;
	strlcpy(codec->client.name, DRIVER_NAME, I2C_NAME_SIZE);
	ret = i2c_attach_client(&codec->client);
	if(ret != 0) {
		LOG_ERR("Can't attach client to i2c bus on address 0x%02x\n", address);
		goto err_attach_client;
	}
	LOG_INFO("Probe finish OK\n");
	codecs[address-0x18] = codec;

	if(master_codec == -1) {
		ret = codec_init(codec, 1, (address-0x18) * 32);
		if(ret == 0) master_codec = address-0x18;
	} else {
		codec_init(codec, 0, (address-0x18) * 32);
	}
	if(ret != 0) {
		LOG_ERR("Can't initialize ADC3101 codec on address 0x%02x. Error: %d\n", address, ret);
		goto err_codec_init;
	}
	return 0;

err_codec_init:
	codec_reset(codec);
	i2c_detach_client(&codec->client);
err_attach_client:
	kfree(codec);
err_kmalloc_codec:
	return ret;
}

static int /*__init*/ tlv320adc3101_attach_adapter(struct i2c_adapter *bus) {
	LOG_INFO("tlv320adc3101_scan_bus\n");
	if(!i2c_check_functionality(bus, I2C_FUNC_SMBUS_BYTE_DATA)) return -EINVAL;
	return i2c_probe(bus, &addr_data, tlv320adc3101_probe);
}

static int tlv320adc3101_detach_client(struct i2c_client *client) {
	struct codec *codec;

	codec = container_of(client, struct codec, client);

	LOG_INFO("tlv320adc3101_detach_client on address 0x%02x\n", client->addr);

	i2c_detach_client(client);
	kfree(codec);

	return 0;
}

static int tlv320adc3101_detach_adapter(struct i2c_adapter *bus) {
	int i;
	LOG_INFO("tlv320adc3101_detach_adapter\n");
	for(i = 0; i < 4; i++) {
		if(codecs[i] != 0) {
			tlv320adc3101_detach_client(&codecs[i]->client);
			codecs[i] = 0;
		}
	}
	return 0;
}

static struct i2c_driver tlv320adc3101_driver = {
	.driver = {
		.name = DRIVER_NAME
	},
	.attach_adapter = tlv320adc3101_attach_adapter,
	.detach_adapter = tlv320adc3101_detach_adapter,
//	.detach_client = tlv320adc3101_detach_client,
};

int tlv320adc3101_init(void) {
	u32 tries = 1;
	int status = -ENODEV;
	LOG_INFO("TLV320ADC3101 driver version: "VERSION", svn: %d\n", SVN);

	while(tries--) {
		LOG_INFO("i2c tries: %d\n", tries);
		status = i2c_add_driver(&tlv320adc3101_driver);
		if(init_codecs != 0x0) break;
		LOG_INFO("init_codecs: 0x%02x\n", init_codecs);
		i2c_del_driver(&tlv320adc3101_driver);
		if(!tries) {
			LOG_ERR("%s: no TLV320ADC3101 chip?\n", DRIVER_NAME);
			return -ENODEV;
		}
		msleep(10);
	}
	LOG_INFO("%s: init success. Init codecs: 0x%02x\n", DRIVER_NAME, init_codecs);
	return 0;
}

void tlv320adc3101_exit(void) {
	LOG_INFO("TLV320ADC3101 exit\n");
	i2c_del_driver(&tlv320adc3101_driver);
}

