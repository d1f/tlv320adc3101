#include <linux/kernel.h>
#include <linux/module.h>

#include <linux/spinlock.h>
#include <linux/uaccess.h>
#include <linux/clk.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/platform_device.h>
#include <linux/ioport.h>
#include <linux/delay.h>
#include <linux/poll.h>
#include <linux/dma-mapping.h>

#include <asm/arch/hardware.h>
#include <asm/arch/gpio.h>
#include <asm/arch/irqs.h>
#include <asm/arch/edma.h>
#include <asm/arch/clock.h>

#include "logger.h"
#include "McBSP.h"
#include "tlv320adc3101.h"

//#include "ioctl.h"
#include "audio_ioctl.h"

int log_shift;

MODULE_LICENSE("GPL");
#define VERSION "0.1"

static int cfg_num_streams = 8;
module_param(cfg_num_streams, int, 0);
MODULE_PARM_DESC(cfg_num_streams, "Count of ADC3101");

/*static struct resource dm365_evm_snd_resources[] = {
	{
		.start = DM365_MCBSP_BASE,
		.end = DM365_MCBSP_BASE + SZ_8K - 1,
		.flags = IORESOURCE_MEM,
	},
};*/

#define DRIVER_NAME	"audio"

volatile struct McBSP_regs *regs = (struct McBSP_regs*)IO_ADDRESS(DM365_MCBSP_BASE);
static struct clk *clock = NULL;

static dev_t devno;
static struct cdev c_dev;
static unsigned int audio_major = 0;
static unsigned int audio_minor = 0;

extern int init_codecs;

#define MAX_CHANNELS    8
#define BUFFER_LENGTH	1024
#define DATA_SIZE		2
static int num_channels;
static int master_lch;				// Logical EDMA channel for read data from McBSP.
static int slave0_lch, slave1_lch;
//int sort_lch;
//struct paramentry_descriptor sort_paramentry;
//DECLARE_MUTEX(buffers_lock);
//spinlock_t buffers_lock;	// Spinlock for guard buffers array.
struct buffer {
	enum BUFFER_STATE {BUFFER_WRITE, BUFFER_READ, BUFFER_WAIT, BUFFER_FREE} state;
	int id;
	void *area;
	dma_addr_t addr;
	size_t length;
	unsigned int position;
} buffers[2];
static int buffer_write_index = 1;		// Index of buffer used for write data from McBSP
static int buffer_read_index = 0;			// Index of buffer used for read data (to file)
//int buffer_read_id = -1;

static int last_buffer_id = 0;				// Buffer ID for next write buffer
DECLARE_WAIT_QUEUE_HEAD(buffers_queue);

//struct workqueue_struct *read_workqueue = NULL;
//struct work_struct read_work;
static void audio_tasklet_func(unsigned long arg);
DECLARE_TASKLET(audio_tasklet, audio_tasklet_func, 0);

static void audio_tasklet_func(unsigned long arg) {
	struct buffer *read_buffer, *write_buffer;
//	LOG_INFO("Complete DMA logical channel: %d\n", buffers[buffer_write_index].id);

	buffer_read_index = buffer_write_index;
	read_buffer = &buffers[buffer_read_index];
	read_buffer->state = BUFFER_READ;

	buffer_write_index = (1-buffer_write_index);
	write_buffer = &buffers[buffer_write_index];
	write_buffer->state = BUFFER_WRITE;
	write_buffer->id = last_buffer_id++;
	wake_up_interruptible(&buffers_queue);
}

void init_dma_channel(int lch, int addr) {
	davinci_set_dma_src_params(lch, DM365_MCBSP_BASE+0x00, FIFO, W16BIT);
	davinci_set_dma_dest_params(lch, addr, INCR, W8BIT);
	davinci_set_dma_src_index(lch, 0, 0);
	davinci_set_dma_dest_index(lch, DATA_SIZE, 0);
	davinci_set_dma_transfer_params(lch, DATA_SIZE, num_channels*BUFFER_LENGTH, 1, num_channels*BUFFER_LENGTH, ASYNC);
/*	davinci_set_dma_src_params(lch, DM365_MCBSP_BASE+0x00, FIFO, W16BIT);
	davinci_set_dma_dest_params(lch, addr, INCR, W8BIT);
	davinci_set_dma_src_index(lch, 0, 0);
	davinci_set_dma_dest_index(lch, BUFFER_LENGTH, -(num_channels-1)*BUFFER_LENGTH+2);
	davinci_set_dma_transfer_params(lch, 2, num_channels, BUFFER_LENGTH/2, num_channels, ASYNC);*/
}

void dma_prepare(void) {
	struct paramentry_descriptor temp;
//	struct buffer *write_buffer = NULL, *read_buffer = NULL, *wait_buffer = NULL;
//	LOG_INFO("dma_prepare: %d, write: %4d, position: %8d\n",
//			buffer_write_index, last_buffer_id, last_buffer_position);
//	spin_lock(&buffers_lock);
//	down(&buffers_lock);
/*	if(buffer_write_index == -1) {
		buffer_write_index = 0;
	} else {
		if(buffer_read_index != -1) {
			wait_buffer = &buffers[buffer_read_index];
			wait_buffer->state = BUFFER_WAIT;
		}
		buffer_read_index = buffer_write_index;
		buffer_read_id = buffers[buffer_read_index].id;
		read_buffer = &buffers[buffer_read_index];
		read_buffer->state = BUFFER_READ;
		buffer_write_index = (buffer_write_index+1)%BUFFER_COUNT;
	}
	write_buffer = &buffers[buffer_write_index];
	write_buffer->state = BUFFER_WRITE;
	write_buffer->id = last_buffer_id++;
	last_buffer_position += BUFFER_LENGTH/2;
	write_buffer->position = last_buffer_position;
*/

	init_dma_channel(slave0_lch, buffers[0].addr);
	init_dma_channel(slave1_lch, buffers[1].addr);

	davinci_get_dma_params(slave1_lch, &temp);
	davinci_set_dma_params(master_lch, &temp);

	davinci_dma_link_lch(master_lch, slave0_lch);
	davinci_dma_link_lch(slave0_lch, slave1_lch);
	davinci_dma_link_lch(slave1_lch, slave0_lch);

//	davinci_set_dma_dest_index(slave0_lch, 2, 2);
//	davinci_set_dma_transfer_params(slave0_lch, 2, BUFFER_LENGTH/2, num_channels, BUFFER_LENGTH/2, ASYNC);

//	wake_up_interruptible(&buffers_queue);
//	spin_unlock(&buffers_lock);
//	up(&buffers_lock);
//	LOG_INFO("dma_prepare: read_id: %4d, data: %08x %08x\n",
//			buffer_read_id, ((int*)read_buffer->area)[0], ((int*)read_buffer->area)[1]);
}

int init_buffer(struct device *dev, struct buffer *buffer, int id) {
	int ret = 0;
	buffer->state = BUFFER_WAIT;
	buffer->id = id;
	buffer->length = BUFFER_LENGTH*num_channels*DATA_SIZE;
	buffer->area = dma_alloc_writecombine(dev, buffer->length, &buffer->addr, GFP_KERNEL);
	if(buffer->area == 0) ret = -ENOMEM;
	return ret;
}
void free_buffer(struct device *dev, struct buffer *buffer) {
	dma_free_writecombine(dev, buffer->length, buffer->area, buffer->addr);
	buffer->state = BUFFER_FREE;
}
int init_buffers(struct device *dev) {
	int i;
	int ret = 0;
	for(i = 0; i < 2; i++) {
		if((ret = init_buffer(dev, &buffers[i], last_buffer_id++)) != 0) break;
	}
	if(ret != 0) for(i--; i >= 0; i++) free_buffer(dev, &buffers[i]);
	return ret;
}
void free_buffers(struct device *dev) {
	int i;
	for(i = 0; i < 2; i++) free_buffer(dev, &buffers[i]);
}


struct channel_config {
	int input;
	int _6dB;
	int noisedetect;
	int noiselevel;
};
struct channel_device {
	char name[20];
	int intr_complete;
	int id;
	dev_t devno;
	wait_queue_head_t intr_wait;
	wait_queue_head_t read_wait;
	char *buffer;
	int buffer_length;
	int buffer_max_length;
//	struct control controls[ARRAY_SIZE(controls)];
	struct channel_config configs[MAX_CHANNELS];
} audioChannel;

struct file_private {
	struct channel_device *channel;
	size_t offset;
	int buffer_id;
//	int buffer_index;
};

static struct class *audio_class;

int tlv320adc3101_init(void);
void tlv320adc3101_exit(void);

int audio_open(struct inode *inode, struct file *file) {
	struct channel_device *channel = &audioChannel;
	struct file_private *fpriv = NULL;
//	LOG_INFO("Open file with minor: %d\n", iminor(inode));
	if(IS_ERR(channel)) {
		LOG_ERR("Try to open not exist audio channel %d\n", iminor(inode));
		return PTR_ERR(channel);
	}
	fpriv = (struct file_private*)kmalloc(sizeof(struct file_private), GFP_KERNEL);
	if(fpriv == NULL) {
		LOG_ERR("Can't create file private structure\n");
		return -ENOMEM;
	}
	memset(fpriv, 0, sizeof(struct file_private));
	fpriv->channel = channel;
	fpriv->offset = -1;
	fpriv->buffer_id = -1;
	//fpriv->buffer_index = -1;
	file->private_data = fpriv;
//	LOG_INFO("Open audio file: \"%s\"\n", channel->name);
	return 0;
}

int audio_release(struct inode *inode, struct file *file) {
	struct file_private *fpriv = (struct file_private*)file->private_data;
//	LOG_INFO("Close audio file: \"%s\"\n", fpriv->channel->name);
	kfree(fpriv);
	return 0;
}

ssize_t audio_read(struct file *file, char *buff, size_t size, loff_t *offset) {
	void *address = NULL;
	size_t count = 0;
	struct file_private *fpriv = (struct file_private*)file->private_data;
	while(address == NULL) {
		struct buffer *read_buffer;
		tasklet_disable(&audio_tasklet);
		read_buffer = &buffers[buffer_read_index];
		if(fpriv->buffer_id == -1) {
			fpriv->buffer_id = read_buffer->id;
			fpriv->offset = BUFFER_LENGTH*DATA_SIZE*num_channels;
		} else {
			if(read_buffer->id != fpriv->buffer_id) {
				size_t loss = (read_buffer->id - fpriv->buffer_id)*DATA_SIZE*BUFFER_LENGTH*num_channels - fpriv->offset;
				// Miss data condition (read_buffer changed from last read)
				if(loss != 0) LOG_INFO("audio_read(%s): data loss %d\n", fpriv->channel->name, loss);
				fpriv->buffer_id = read_buffer->id;
				fpriv->offset = 0;
				address = (void*)((uint32_t)read_buffer->area /*+ fpriv->channel->id*BUFFER_LENGTH*/);
				count = min(size, (size_t)BUFFER_LENGTH*DATA_SIZE*num_channels);
			} else {
				// Normal condition (next read data from this buffer)
				if(fpriv->offset < BUFFER_LENGTH*DATA_SIZE*num_channels) {
					// Data availible
					address = (void*)((uint32_t)read_buffer->area +
							/*fpriv->channel->id*BUFFER_LENGTH +*/ fpriv->offset);
					count = min(size, (size_t)BUFFER_LENGTH*DATA_SIZE*num_channels-fpriv->offset);
				} else {} // No data availible
			}
		}
		tasklet_enable(&audio_tasklet);
		if(address == NULL) {
			if(file->f_flags & O_NONBLOCK) return -EAGAIN;
			if(wait_event_interruptible(buffers_queue, buffers[buffer_read_index].id != fpriv->buffer_id)) {
				return -ERESTARTSYS;
			}
		}
	}
/*	LOG_INFO("audio_read(%s): count: %5d, offset: %4d, id: %5d, data: %08x %08x\n",
			fpriv->channel->name, count, fpriv->offset, fpriv->buffer_id, ((int*)address)[0], ((int*)address)[1]);*/
	if(copy_to_user(buff, address/*+BUFFER_LENGTH*num_channels*/, count)) return -EINVAL;
//	memset(address, 0x7F, count);
	fpriv->offset += count;
	*offset += count;
	return count;
}

unsigned int audio_poll(struct file *file, poll_table *wait) {
	unsigned int ret = 0;
	struct file_private *fpriv = (struct file_private*)file->private_data;
	poll_wait(file, &buffers_queue, wait);
	if(buffers[buffer_read_index].id != fpriv->buffer_id) ret |= POLLIN | POLLRDNORM;
	return ret;
}

#define ABS(a) ((a) < 0 ? -(a) : (a))
int get_AGCattackdecay(int ms) {
	int ret = 0, error = ABS(ms-1);
	int i;
	for(i = 1; i < 255; i++) {
		int time = ((i >> 3) << 1) | 1;
		int factor = i & 7;
		int err = ABS(ms - (time << factor));
		if(err < error) error = err, ret = i;
	}
	return ret;
}

int audio_ioctl(struct inode *inode, struct file *file, unsigned int cmd, unsigned long arg) {
	struct file_private *fpriv = (struct file_private*)file->private_data;
	struct channel_device *channel = fpriv->channel;
	struct channel_config *config;
	int ret = 0;
//	int index;
//	int32_t value;
	int ch = AUDIO_IOCTL_CHANNEL(cmd);
	int op = AUDIO_IOCTL_OPERATION(cmd);
	if(!AUDIO_IOCTL_VALID(cmd)) return -EINVAL;
	if(ch >= num_channels) return -EINVAL;
	config = &channel->configs[ch];
	switch(op) {
		case IOCTL_SET_INPUT:
			tlv320adc3101_set_input_mode(ch, 1, 3);
			tlv320adc3101_set_input_mode(ch, 2, 3);
			tlv320adc3101_set_input_mode(ch, 3, 3);
			tlv320adc3101_set_input_mode(ch, 4, 3);
			switch(arg) {
				case 0:		// differential
					tlv320adc3101_set_input_mode(ch, 1, config->_6dB); break;
				case 1:		// phantom
					tlv320adc3101_set_input_mode(ch, 2, config->_6dB);
					tlv320adc3101_set_input_mode(ch, 3, config->_6dB); break;
				case 2:		// line
					tlv320adc3101_set_input_mode(ch, 4, config->_6dB); break;
				default:
					ret = -EINVAL;
			}
			if(ret == 0) config->input = arg;
			break;
		case IOCTL_SET_GAIN:
			ret = tlv320adc3101_set_gain(ch, arg); break;
		case IOCTL_SET_6dB:
			arg = (arg == 0)?0:1;
			tlv320adc3101_set_input_mode(ch, 1, 3);
			tlv320adc3101_set_input_mode(ch, 2, 3);
			tlv320adc3101_set_input_mode(ch, 3, 3);
			tlv320adc3101_set_input_mode(ch, 4, 3);
			switch(config->input) {
				case 0:		// differential
					tlv320adc3101_set_input_mode(ch, 1, arg); break;
				case 1:		// phantom
					tlv320adc3101_set_input_mode(ch, 2, arg);
					tlv320adc3101_set_input_mode(ch, 3, arg); break;
				case 2:		// line
					tlv320adc3101_set_input_mode(ch, 4, arg); break;
				default:
					ret = -EINVAL;
			}
			if(ret == 0) config->_6dB = arg;
			break;
		case IOCTL_SET_DCOFFSET:
			ret = tlv320adc3101_set_dc_offset(ch, arg); break;
		case IOCTL_SET_VOLUME:
			ret = tlv320adc3101_set_volume(ch, arg); break;
		case IOCTL_SET_MUTE:
			ret = tlv320adc3101_set_mute(ch, arg); break;
		case IOCTL_SET_AGC:
			ret = tlv320adc3101_set_AGC(ch, arg); break;
		case IOCTL_SET_AGCLEVEL:
			ret = tlv320adc3101_set_AGC_level(ch, arg); break;
		case IOCTL_SET_AGCHYST:
			ret = tlv320adc3101_set_AGC_hyst(ch, arg); break;
		case IOCTL_SET_AGCMAXGAIN:
			ret = tlv320adc3101_set_AGC_max_gain(ch, arg); break;
		case IOCTL_SET_AGCNOISEDEBOUNCE:
			ret = tlv320adc3101_set_AGC_noise_debounce(ch, arg); break;
		case IOCTL_SET_AGCSIGNALDEBOUNCE:
			ret = tlv320adc3101_set_AGC_signal_debounce(ch, arg); break;
		case IOCTL_SET_AGCATTACK:
			{
				int val = get_AGCattackdecay(arg/2);
				ret = tlv320adc3101_set_AGC_attack(channel->id, val);
				break;
			}
		case IOCTL_SET_AGCDECAY:
			{
				int val = get_AGCattackdecay(arg/32);
				ret = tlv320adc3101_set_AGC_decay(channel->id, val);
				break;
			}
		case IOCTL_SET_AGCNOISEDETECT:
			config->noisedetect = arg;
			ret = tlv320adc3101_set_AGC_noise_level(ch, config->noisedetect ? config->noiselevel : 0);
			break;
		case IOCTL_SET_AGCNOISELEVEL:
			if(arg < 1 || arg > 0x1F) ret = -EINVAL;
			else {
				config->noiselevel = arg;
				ret = tlv320adc3101_set_AGC_noise_level(ch, config->noisedetect ? config->noiselevel : 0);
			}
			break;
		case IOCTL_GET_AGCGAIN:
			{
				int gain;
				ret = tlv320adc3101_get_AGC_gain(ch, &gain);
				if(ret == 0) copy_to_user((void*)arg, &gain, sizeof(gain));
				break;
			}
		case IOCTL_SET_MICBIAS:
			ret = tlv320adc3101_set_micbias(ch, arg);
			break;
		case IOCTL_SET_SAMPLE_RATE:
			ret = tlv320adc3101_set_sample_rate(ch, arg);
			break;
		case IOCTL_SET_I2C:
			{
				audio_i2c_t i2c;
				if ( copy_from_user(&i2c, (void*)arg, sizeof(i2c)) )
					return -EFAULT;
				ret = tlv320adc3101_set_i2c(i2c.page, i2c.addr, i2c.value);
			}
			break;
		case IOCTL_GET_I2C:
			{
				audio_i2c_t i2c;
				if ( copy_from_user(&i2c, (void*)arg, sizeof(i2c)) )
					return -EFAULT;

				ret = tlv320adc3101_get_i2c(i2c.page, i2c.addr, &i2c.value);

				if ( copy_to_user((void*)arg, &i2c, sizeof(i2c)) )
					return -EFAULT;
			}
			break;

		default: ret = -EINVAL;
	}
	if(ret < 0) LOG_ERR("audio IOCTL: error %d on 0x%08x cmd with 0x%08lx arg\n", ret, cmd, arg);
	return ret;
}

static struct file_operations audio_fops = {
	.owner = THIS_MODULE,
	.read = audio_read,
	.open = audio_open,
	.ioctl = audio_ioctl,
	.poll = audio_poll,
	.release = audio_release,
};

#define CHAR_MAJOR 245
#define CHAR_FILENAME "audio_mono"

static irqreturn_t rint_isr(int irq, void *dev_id, struct pt_regs *regs_t) {
//	LOG_INFO("0x%04x\n", regs->DRR);
	LOG_ERR("irq: RSYNCERR\n");
	return IRQ_HANDLED;
}

static void audio_dma_irq(int lch, u16 ch_status, void *data) {
	if(ch_status != DMA_COMPLETE) {return;}
	tasklet_schedule(&audio_tasklet);
}

static int __init audio_dev_init(void) {
	int ret;
	struct class_device *cdevice;
//	unsigned long rate;
//	int devider;
//	const int bclk = 9000000;

    num_channels = cfg_num_streams * 2;
    if(num_channels == 0) num_channels = 2;
    if(num_channels > MAX_CHANNELS) num_channels = MAX_CHANNELS;
	LOG_INFO("Audio driver for AVK start with %d channels. Version: "VERSION", svn: %d\n", num_channels, SVN);

//	clock = clk_get(NULL, MCBSP_CLK_NAME);
//	rate = clk_get_rate(clock);
//	devider = rate/bclk - 1;
//	LOG_INFO("Audio clock: %ld, devider: %d (%s)\n", rate, devider, ((devider+1)*bclk == rate)?"ERROR":"OK");

	// Reset all devices
	regs->SPCR = 0; //&= ~(SPCR_RRST | SPCR_XRST | SPCR_GRST | SPCR_FRST);

	// Configure device pins (input and output)
	// Receive data sampled on failing edge of CLKR (PCR_CLKRP=0)
	// Receive frame-synchronization pulse is active high (PCR_FSRP=0)
	// CLKS clock (PCR_SCLKME=0, SPCR_CLKSM=0)
	// CLKR is an input pin (SPCR_DLB=0->PCR_CLKRM=1)
	// Frame synchronization is get from codec (PCR_FSRM=0)
	regs->PCR = 0;//PCR_CLKRM | PCR_FSRM;

	// Configure clocks and frames
	// CLKGDV = 30
	// Frame width = 1 (unused?)
	// Frame period = 16*4 (unised?)
	// SRGR_CLKSM=1
	// CLKSP & GSYNC unused
//	regs->SRGR = SRGR_CLKGDIV(6-1) | SRGR_FWID(1-1) | SRGR_FPER(128*2-1);
//	regs->SPCR |= SPCR_GRST | SPCR_FRST;

	// Configure interrupts from RINT by RSYNCERR
	regs->SPCR |= SPCR_RINTM(3);

	// Configure interrupts
	ret = request_irq(IRQ_DM365_MBRINT, rint_isr, SA_INTERRUPT, DRIVER_NAME, &audioChannel);
	if(ret != 0) {
		LOG_ERR("Can't request interrupt %d (RINT)\n", IRQ_DM365_MBRINT);
		ret = -ENODEV;
		goto err_request_irq;
	}
//	disable_irq(IRQ_DM365_MBRINT);

	// Configure DMA
	ret = init_buffers(NULL);
	if(ret != 0) {
		LOG_ERR("Can't initialize buffers for DMA\n");
		goto err_init_buffers;
	}
	{
		int tcc = EDMA_TCC_ANY;
		ret = davinci_request_dma(DM365_DMA_MCBSP_RX, "McBSP_RX", audio_dma_irq, NULL,
				&master_lch, &tcc, EVENTQ_3);
		if(ret) {
			LOG_ERR("Can't request DMA channel\n");
			goto err_request_dma;
		}
		ret = davinci_request_dma(DAVINCI_EDMA_PARAM_ANY, "McBSP_RX_link0", audio_dma_irq, NULL,
				&slave0_lch, &tcc, EVENTQ_3);
		if(ret) {
			LOG_ERR("Can't request DMA slave0 channel\n");
			goto err_request_dma_slave0;
		}
		ret = davinci_request_dma(DAVINCI_EDMA_PARAM_ANY, "McBSP_RX_link1", audio_dma_irq, NULL,
				&slave1_lch, &tcc, EVENTQ_3);
		if(ret) {
			LOG_ERR("Can't request DMA slave1 channel\n");
			goto err_request_dma_slave1;
		}
//		davinci_dma_link_lch(slave_lch, slave_lch);

/*		tcc = EDMA_TCC_ANY;
		ret = davinci_request_dma(EDMA_DMA_CHANNEL_ANY, "McBSP_sort", audio_sort_irq, NULL,
				&sort_lch, &tcc, EVENTQ_1);
		if(ret) {
			LOG_ERR("Can't request DMA sort channel\n");
			goto err_request_dma_sort;
		}
		LOG_INFO("sort_lch %d\n", sort_lch);
		davinci_set_dma_src_params(sort_lch, 0, INCR, W8BIT);
		davinci_set_dma_dest_params(sort_lch, 0, INCR, W8BIT);
		davinci_set_dma_src_index(sort_lch, num_channels*2, 2);
		davinci_set_dma_dest_index(sort_lch, 2, BUFFER_LENGTH/2*2);
		davinci_set_dma_transfer_params(sort_lch, 2, BUFFER_LENGTH/2, num_channels, BUFFER_LENGTH/2, ABSYNC);
		davinci_dma_chain_lch(sort_lch, sort_lch);
		davinci_get_dma_params(sort_lch, &sort_paramentry);
		sort_paramentry.opt &= ~TCCHEN;
		sort_paramentry.opt |= ITCCHEN;*/
	}

	// Configure phase length and word length
	// One phase mode (RCR_RPHASE=0)
	// Data delay is 0;
	// Word length in phase 1: 16bit
	// Words count in phase 1: num_channels words
	regs->RCR = RCR_RWDLEN1(WDLEN_16) | RCR_RFRLEN1(num_channels-1);

#if 0
	// Init workqueue and work structures
	read_workqueue = create_workqueue("McBSP_read");
	if(read_workqueue == NULL) {
		LOG_ERR("Can't create workqueue structure\n");
		ret = -ENOMEM;
		goto err_create_workqueue;
	}
	INIT_WORK(&read_work, read_work_func, NULL);
#endif

	ret = tlv320adc3101_init();
	if(ret != 0) {
		LOG_ERR("Can't initialize tlv320adc3101 modules\n");
		goto err_tlv320adc3101_init;
	}

	ret = alloc_chrdev_region(&devno, 0, 1, DRIVER_NAME);
	if(ret < 0) {
		LOG_ERR("Audio: Can't register character devices\n");
		ret = -ENODEV;
		goto err_alloc_chrdev_region;
	}

	audio_major = MAJOR(devno);
	audio_minor = MINOR(devno);

	cdev_init(&c_dev, &audio_fops);
	c_dev.owner = THIS_MODULE;
	c_dev.ops = &audio_fops;
	ret = cdev_add(&c_dev, devno, num_channels);
	if(ret) {
		LOG_ERR("Error adding Audio\n");
		goto err_cdev_add;
	}

	audio_class = class_create(THIS_MODULE, "audio");
	if(!audio_class) {
		LOG_ERR("Can't create class \"audio\"\n");
		goto err_class_create;
	}

	/*for(i = 0; i < num_channels; i++) {
		int j;
		struct class_device *cdevice;
//		if(init_codecs & (1 << (i/2))) {
		{
			channels[i].id = i;
			channels[i].devno = MKDEV(audio_major, audio_minor+i);
			memcpy(channels[i].controls, controls, sizeof(controls));
			init_waitqueue_head(&channels[i].intr_wait);
			init_waitqueue_head(&channels[i].read_wait);
			sprintf(channels[i].name, "audio%d", i);
			cdevice = class_device_create(audio_class, NULL, channels[i].devno, NULL, "audio%d", i);
			if(IS_ERR(cdevice)) {
				LOG_ERR("Can't create class_device to %d channel\n", i);
				ret = PTR_ERR(cdevice);
				break;
			}
			for(j = 0; j < ARRAY_SIZE(channels[i].controls); j++) {
				if(!(channels[i].controls[j].flags & FLAG_RO)) {
					if(control_set_funcs[j] != NULL) {
						if(0 != control_set_funcs[j](&channels[i], j, channels[i].controls[j].def_value)) {
							LOG_ERR("Can't set control \"%s\" on channel \"%s\" to %d\n",
									channels[i].controls[j].name, channels[i].name, channels[i].controls[j].def_value);
						}
					}
				}
			}
		}
	}
	if(i < num_channels) {
		for(i--; i >= 0; i--) {
			class_device_destroy(audio_class, channels[i].devno);
		}
		goto err_class_device_create;
	}
	*/
	audioChannel.id = 0;
	audioChannel.devno = MKDEV(audio_major, audio_minor);
//	memcpy(audioChannel.controls, controls, sizeof(controls));
	init_waitqueue_head(&audioChannel.intr_wait);
	init_waitqueue_head(&audioChannel.read_wait);
	sprintf(audioChannel.name, "audio");
	cdevice = class_device_create(audio_class, NULL, audioChannel.devno, NULL, "audio");
	if(IS_ERR(cdevice)) {
		LOG_ERR("Can't create class_device\n");
		ret = PTR_ERR(cdevice);
		goto err_class_device_create;
	}
/*	{
		int j;
		for(j = 0; j < ARRAY_SIZE(audioChannel.controls); j++) {
			if(!(audioChannel.controls[j].flags & FLAG_RO)) {
				if(control_set_funcs[j] != NULL) {
					if(0 != control_set_funcs[j](&audioChannel, j, audioChannel.controls[j].def_value)) {
						LOG_ERR("Can't set control \"%s\" on channel \"%s\" to %d\n",
								audioChannel.controls[j].name, audioChannel.name, audioChannel.controls[j].def_value);
					}
				}
			}
		}
	}*/

	// Start DMA
	dma_prepare();
	{
		ret = davinci_start_dma(master_lch);
		LOG_INFO("davinci_start_dma return %d\n", ret);
	}

	// Start receiver
	regs->SPCR |= /*SPCR_GRST | SPCR_FRST |*/ SPCR_RRST;

	{
		int ch;
		for(ch = 0; ch < num_channels; ch++) {
			tlv320adc3101_set_input_mode(ch, 1, 0);
			tlv320adc3101_set_input_mode(ch, 2, 3);
			tlv320adc3101_set_input_mode(ch, 3, 3);
			tlv320adc3101_set_input_mode(ch, 4, 3);
			tlv320adc3101_set_mute(ch, 0);
		}
	}
	return 0;


//	for(i = 0; i < num_channels; i++) {
		class_device_destroy(audio_class, audioChannel.devno);
//	}
err_class_device_create:
	class_destroy(audio_class);
err_class_create:
	cdev_del(&c_dev);
err_cdev_add:
	unregister_chrdev_region(devno, 1/*num_channels*/);
err_alloc_chrdev_region:
	tlv320adc3101_exit();
err_tlv320adc3101_init:

//	destroy_workqueue(read_workqueue);
//err_create_workqueue:
//	davinci_dma_unlink_lch(slave_lch, slave_lch);
//	davinci_dma_unchain_lch(sort_lch, sort_lch);
//	davinci_free_dma(sort_lch);
//err_request_dma_sort:
	davinci_free_dma(slave1_lch);
err_request_dma_slave1:
	davinci_free_dma(slave0_lch);
err_request_dma_slave0:
	davinci_free_dma(master_lch);
err_request_dma:
	free_buffers(NULL);
err_init_buffers:
	free_irq(IRQ_DM365_MBRINT, &audioChannel);
err_request_irq:
	regs->SPCR &= ~(SPCR_RRST | SPCR_XRST | SPCR_GRST | SPCR_FRST);
//err_get_pca:
	return ret;
}
module_init(audio_dev_init);

static void __exit audio_dev_exit(void) {
	LOG_INFO("Audio driver for AVK_01 module exit\n");

	davinci_stop_dma(master_lch);

//	for(i = 0; i < num_channels; i++) {
		class_device_destroy(audio_class, audioChannel.devno);
//	}
	class_destroy(audio_class);
	cdev_del(&c_dev);
	unregister_chrdev_region(devno, 1/*num_channels*/);
	tlv320adc3101_exit();

//	flush_workqueue(read_workqueue);
//	destroy_workqueue(read_workqueue);
	tasklet_kill(&audio_tasklet);

	davinci_dma_unlink_lch(master_lch, slave0_lch);
	davinci_dma_unlink_lch(slave0_lch, slave1_lch);
	davinci_dma_unlink_lch(slave1_lch, slave0_lch);
	davinci_free_dma(slave1_lch);
	davinci_free_dma(slave0_lch);
	davinci_free_dma(master_lch);
	free_buffers(NULL);
	free_irq(IRQ_DM365_MBRINT, &audioChannel);

	// Reset all devices
	regs->SPCR &= ~(SPCR_RRST | SPCR_XRST | SPCR_GRST | SPCR_FRST);
	clk_put(clock);
}

module_exit(audio_dev_exit);

