#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <linux/types.h>
#include <string.h>
#include <unistd.h> // read

#include "audio_ioctl.h"

static const char *filename = "/dev/audio";
static int file;

#define NUM_CHANNELS	8
#define BUFFER_LENGTH	8192
short buffer[BUFFER_LENGTH][NUM_CHANNELS];
short outbuffer[BUFFER_LENGTH];

int main(int argc, char *argv[]) {
	(void)argc; (void)argv;
	int i;
	int ret = 0;

	file = open(filename, O_RDONLY);
	if(file < 0) {
		perror("Can't open file\n");
		return -1;
	}

	for(i = 0; i < NUM_CHANNELS; i++) {
		fprintf(stderr, "Channel %d\n", i);
		ret = ioctl(file, AUDIO_IOCTL(i, IOCTL_SET_INPUT), 0);
		if(ret != 0) perror("Can't set input");
		ret = ioctl(file, AUDIO_IOCTL(i, IOCTL_SET_MUTE), 0);
		if(ret != 0) perror("Can't set mute off");
	}

	while(1) {
		ssize_t readen = read(file, buffer, sizeof(buffer));
		fprintf(stderr, "Read %d/%d bytes. %d samples\n", readen, sizeof(buffer), readen/sizeof(buffer[0]));
		for(i = 0; (size_t)i < readen / sizeof(buffer[0]); i++) {
			outbuffer[i] = buffer[i][0];
		}
		write(fileno(stdout), outbuffer, 2*readen/sizeof(buffer[0]));
//		write(fileno(stdout), buffer, readen);
	}

	close(file);
	return 0;
}

