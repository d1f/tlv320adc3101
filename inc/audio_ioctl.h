#ifndef __audio_ioctlH__
#define __audio_ioctlH__

#define IOCTL_GET_VERSION			0x0001
#define IOCTL_SET_INPUT				0x0010
#define IOCTL_SET_GAIN				0x0011
#define IOCTL_SET_6dB				0x0012
#define IOCTL_SET_DCOFFSET			0x0013
#define IOCTL_SET_VOLUME			0x0014
#define IOCTL_SET_MUTE				0x0015
#define IOCTL_SET_AGC				0x0016
#define IOCTL_SET_AGCLEVEL			0x0017
#define IOCTL_SET_AGCHYST			0x0018
#define IOCTL_SET_AGCMAXGAIN			0x0019
#define IOCTL_SET_AGCNOISEDEBOUNCE		0x001A
#define IOCTL_SET_AGCSIGNALDEBOUNCE		0x001B
#define IOCTL_SET_AGCATTACK			0x001C
#define IOCTL_SET_AGCDECAY			0x001D
#define IOCTL_SET_AGCNOISEDETECT		0x001E
#define IOCTL_SET_AGCNOISELEVEL			0x001F
#define IOCTL_GET_AGCGAIN			0x0020
#define IOCTL_SET_MICBIAS			0x0021
#define IOCTL_SET_SAMPLE_RATE			0x0022
#define IOCTL_SET_I2C				0x0030
#define IOCTL_GET_I2C				0x0031

#define AUDIO_MAGIC				0x0ADC
#define AUDIO_IOCTL(ch, cmd)			(((AUDIO_MAGIC)<<16) | ((ch)<<8) | (cmd))
#define AUDIO_IOCTL_VALID(cmd)			((((cmd) >> 16) & 0xFFFF) == AUDIO_MAGIC)
#define AUDIO_IOCTL_CHANNEL(cmd)		(((cmd) >> 8) & 0xFF)
#define AUDIO_IOCTL_OPERATION(cmd)		((cmd) & 0xFF)


typedef struct
{
    unsigned char page;
    unsigned char addr;
    unsigned char value;
} audio_i2c_t;

#endif // __audio_ioctlH__
